﻿
//function dealSummariesCalc(){
//	
//	var dealSummaries = ds.Deal.all().dealSummary,
//		dealSummariesCount = dealSummaries.length,
//		totalSumm = 0;

//	for(var i=0; i < dealSummariesCount; i++){
//		totalSumm += dealSummaries[i];
//	}
//	
//	return totalSumm;
//	
//};


WAF.onAfterInit = function onAfterInit() {// @lock

// @region namespaceDeclaration// @startlock
	var login1 = {};	// @login
	var menuItem6 = {};	// @menuItem
	var menuItem5 = {};	// @menuItem
	var menuItem4 = {};	// @menuItem
// @endregion// @endlock

var applicationLayout = window.document.getElementById('applicationLayout');

$$('applicationLayout').hide();
$$('menuBar2').hide();

//var menuBar = window.document.getElementById('menuBar2');
//applicationLayout.style.opacity = 0.0;
//menuBar.style.opacity = 0.0;

// eventHandlers// @lock
	
	login1.logout = function login1_logout (event)// @startlock
	{// @endlock
//		applicationLayout.style.opacity = 0.0;
//		menuBar.style.opacity = 0.0;
		$$('applicationLayout').hide();
		$$('menuBar2').hide();
	};// @lock

	login1.login = function login1_login (event)// @startlock
	{// @endlock
//		applicationLayout.style.opacity = 1.0;
//		menuBar.style.opacity = 1.0;
		$$('applicationLayout').show();
		$$('menuBar2').show();
		$$('applicationLayout').loadComponent('/views/deal.waComponent');
		$$('applicationLayout_textField1').setValue(dealSummariesCalc());
	};// @lock

	menuItem6.click = function menuItem6_click (event)// @startlock
	{// @endlock
		$$('applicationLayout').loadComponent('/views/staff.waComponent');
	};// @lock

	menuItem5.click = function menuItem5_click (event)// @startlock
	{// @endlock
		$$('applicationLayout').loadComponent('/views/clients.waComponent');
	};// @lock

	menuItem4.click = function menuItem4_click (event)// @startlock
	{// @endlock
		$$('applicationLayout').loadComponent('/views/deal.waComponent');
		$$('applicationLayout_textField1').setValue(dealSummariesCalc());
	};// @lock

// @region eventManager// @startlock
	WAF.addListener("login1", "logout", login1.logout, "WAF");
	WAF.addListener("login1", "login", login1.login, "WAF");
	WAF.addListener("menuItem6", "click", menuItem6.click, "WAF");
	WAF.addListener("menuItem5", "click", menuItem5.click, "WAF");
	WAF.addListener("menuItem4", "click", menuItem4.click, "WAF");
// @endregion
};// @endlock
