﻿
(function Component (id) {// @lock

// Add the code that needs to be shared between components here

function constructor (id) {
	
	var currentTarget,
		currentTargetEmployee;

	// @region beginComponentDeclaration// @startlock
	var $comp = this;
	this.name = 'deal';
	// @endregion// @endlock

	this.load = function (data) {// @lock

	// @region namespaceDeclaration// @startlock
	var button7 = {};	// @button
	var button6 = {};	// @button
	var button5 = {};	// @button
	var button4 = {};	// @button
	var dataGrid1 = {};	// @dataGrid
	// @endregion// @endlock

	// eventHandlers// @lock

	button7.click = function button7_click (event)// @startlock
	{// @endlock
		$$(getHtmlId("dialog2")).closeDialog(); //ok button
		currentTargetEmployee.text($$('applicationLayout_dataGrid3').source.employeeFullname);
		$$('applicationLayout_dataGrid1').source.save();
	};// @lock

	button6.click = function button6_click (event)// @startlock
	{// @endlock
		$$(getHtmlId("dialog2")).closeDialog(); //cancel button
	};// @lock

	button5.click = function button5_click (event)// @startlock
	{// @endlock
		$$(getHtmlId("dialog1")).closeDialog(); //ok button
		currentTarget.text($$('applicationLayout_dataGrid2').source.clientFullName);
		$$('applicationLayout_dataGrid1').source.save();
	};// @lock

	button4.click = function button4_click (event)// @startlock
	{// @endlock
		$$(getHtmlId("dialog1")).closeDialog(); //cancel button
	};// @lock

	dataGrid1.onCellClick = function dataGrid1_onCellClick (event)// @startlock
	{// @endlock
		// Add your code here
//		$$('applicationLayout_textField1').setValue(dealSummariesCalc());
		if(event.data.columnNumber == 2){
			
			$$(getHtmlId("dialog1")).displayDialog();
			currentTarget = $(event.currentTarget);
			
		} 
		else if(event.data.columnNumber == 3) {
						
			$$(getHtmlId("dialog2")).displayDialog();
			currentTargetEmployee = $(event.currentTarget);
			
		}
		
	};// @lock

	dataGrid1.onRowDraw = function dataGrid1_onRowDraw (event)// @startlock
	{// @endlock
		//
	};// @lock

	// @region eventManager// @startlock
	WAF.addListener(this.id + "_button7", "click", button7.click, "WAF");
	WAF.addListener(this.id + "_button6", "click", button6.click, "WAF");
	WAF.addListener(this.id + "_button5", "click", button5.click, "WAF");
	WAF.addListener(this.id + "_button4", "click", button4.click, "WAF");
	WAF.addListener(this.id + "_dataGrid1", "onCellClick", dataGrid1.onCellClick, "WAF");
	WAF.addListener(this.id + "_dataGrid1", "onRowDraw", dataGrid1.onRowDraw, "WAF");
	// @endregion// @endlock

	};// @lock


}// @startlock
return constructor;
})();// @endlock
