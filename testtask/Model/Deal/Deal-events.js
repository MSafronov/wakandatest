
model.Deal.dealDate.onSet = function() {
//	return new Date();
};


model.Deal.dealDate.onGet = function() {
	return new Date();
};


model.Deal.dealTotalOpenedSumm.onGet = function() {
		
	var dealSummaries = ds.Deal.all(),
		dealSummariesCount = dealSummaries.length,
		totalSumm = 0;

	for(var i=0; i < dealSummariesCount; i++){
		if(dealSummaries[i].dealState){
			totalSumm += dealSummaries[i].dealSummary;
		}
	}
	
	return totalSumm;
	
};


model.Deal.events.init = function(event) {
	this.dealDate = new Date();
};
